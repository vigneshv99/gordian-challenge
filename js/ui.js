var cards = [
    {
        typeId: "price",
        title: "Price",
        spacing: "",
        prefix: "$",
        precision: 2,
        suffix: ""
    },
    {
        typeId: "duration_min",
        title: "Duration",
        spacing: "ml-1 mr-1",
        prefix: "",
        precision: 0,
        suffix: " minutes"
    },
    {
        typeId: "stops",
        title: "Stops",
        spacing: "",
        prefix: "",
        precision: 0,
        suffix: ""
    }
]


/*
 * Updates the card in the UI with the specified card
 * config, and the data from processResults.
 */
function updateCard(card, data) {
    var name = card.typeId
    var stats = getStats(name, data)

    var fieldConfigs = [
        {
            val: stats.min,
            name: "Min"
        },
        {
            val: stats.max,
            name: "Max"
        },
        {
            val: stats.average,
            name: "Ave"
        }
    ]

    for (var field of fieldConfigs) {
        $("#" + name + field.name).text(
            card.prefix + field.val.toFixed(card.precision) + card.suffix
        )
        var trace = {
            x: stats.dataArr,
            type: 'histogram',
        };

        var axisTitle = card.title

        if (card.prefix) {
            axisTitle = axisTitle + " (" + card.prefix.trim() + ")"
        } else if (card.suffix) {
            axisTitle = axisTitle + " (" + card.suffix.trim() + ")"
        }

        var layout = {
            autosize: false,
            width: 200,
            height: 200,
            margin: {
                l: 20,
                r: 20,
                b: 40,
                t: 20,
                pad: 1
            },
            xaxis: {
                title: {
                    text: axisTitle,
                },
            }
        }
        var data = [trace];
        Plotly.newPlot(name + "Graph", data, layout, {displayModeBar: false});
    }
}

/*
 * Performs initial page setup, and adds cards.
 */
function setupPage() {
    $("#resultParent").hide()
    $("#spinner").hide()
    $("#errAlert").hide()

    for (var card of cards) {
        var template = $('#card-template')[0].innerHTML;
        var rendered = Mustache.render(template, card);
        $("#results").append(rendered)
    }
}

/*
 * Shows the error alert with the given text.
 */
function showAlert(alertText){
    $("#alert-region").append($("#alert-template")[0].innerHTML)
    $("#alert-text").text(alertText)
}



