var $ = (typeof $ === 'undefined' ? require('jquery') : $);

api_key = "sandbox_gdqCrYULspVetD6Fs2rvnBe32FSaf8DcwhsCQB1oo5npDlfaxMTbHfhu"
base_url = "https://api.gordiansoftware.com/v2.2"

/*
 * Gets the statistics (min, max, average, and data array)
 * for an array of objects and a field.
 *
 * @param name (string): The field to examine.
 * @param data (Array): The array of objects.
 *
 * @returns [Object] An array of objects with fields min, max, average,
 * and dataArr.
 */
function getStats(name, data) {
    minVal = Number.MAX_SAFE_INTEGER
    maxVal = 0
    sum = 0
    dataArr = []

    for (var d of data) {
        minVal = Math.min(minVal, d[name])
        maxVal = Math.max(maxVal, d[name])
        sum += d[name]
        dataArr.push(d[name])
    }

    return {
        min: minVal,
        max: maxVal,
        average: sum / data.length,
        dataArr: dataArr
    }
}

/*
 * Gets the trip creation token, using the global API key,
 * and calls the callback when completed.
 *
 * @param callback(boolean, string), where the first argument is
 * true if the request was successful, and if so, the second argument
 * contains the key.
 */
function getTripCreationToken(callback) {
    $.ajax({
        type: "POST",
        url: base_url + "/authorize",
        beforeSend: function(request) {
            request.setRequestHeader("Authorization", "Basic " + btoa(api_key + ":"))
        },
        success: function(data) {
            if (data.trip_creation_token) {
                callback(true, data.trip_creation_token)
            } else {
                callback(false, "")
            }
        }
    })
}

/*
 * Creates a trip, and searches for tickets matching the specified params.
 * and calls the callback when completed.
 *
 * @param token (string): The trip creation token.
 * @param departure_airport (string): The departure airport code.
 * @param arrival_airport (string): The arrival airport code.
 * @param airline (string): The airline code.
 * @param departure_date (string): The stringified departure date.
 * @param callback(boolean, Object), where the first argument is
 * true if the request was successful, and if so, the second argument
 * contains the response data from the Gordian  API.
 */
function createTrip(
    token,
    departure_airport,
    arrival_airport,
    airline,
    departure_date,
    callback
) {
    // Construct the basic trip, using english and USD.
    var trip = {
        language: "en-US",
        country: "US",
        currency: "USD",
        passengers: [
            {
                type: "adult"
            }
        ],
        search: {
            ticket: {
                journeys: [
                    {
                        departure_airport: departure_airport,
                        arrival_airport: arrival_airport,
                        marketing_airlines: [airline],
                        departure_date: departure_date
                    }
                ]
            }
        }
    }

    // Query the API, and poll the search endpoint once the search is submitted.
    $.ajax({
        type: "POST",
        url: base_url + "/trip",
        data: JSON.stringify(trip),
        dataType: 'json',
        contentType: 'application/json',
        processData: false,
        beforeSend: function(request) {
            request.setRequestHeader("Authorization", "Bearer " + token)
        },
        success: function(data) {
            pollSearch(data.trip_id, data.search_id, data.trip_access_token, callback)
        },
        error: function(error) {
            callback(false, error)
        }
    })
}

/*
 * Polls the search endpoint with the given parameters to search for a ticket.
 * @param trip_id (string): The trip ID to search with.
 * @param search_id (string): The search ID.
 * @param token (string): The trip access token.
 * @param callback(boolean, Object): Will be called on completion of the request.
 * The first parameter will be true if the request succeeds, and the object is the
 * object returned by the Gordian API.
 */
function pollSearch(trip_id, search_id, token, callback) {
    $.ajax({
        type: "GET",
        url: base_url + "/trip/" + trip_id + "/search/" + search_id + "/ticket",
        beforeSend: function(request) {
            request.setRequestHeader("Authorization", "Bearer " + token)
        },
        success: function(data) {
            if (data.status == "success") {
                callback(true, data)
            } else if (data.status == "failed") {
                callback(false, data)
            } else {
                pollSearch(trip_id, search_id, token, callback)
            }
        },
        error: function(data) {
            callback(false, data)
        }
    })
}

/*
 * Processes results from the gordian API into a statistically
 * consumable format.
 *
 * @param data (Object): The data returned by the Gordian API. Should have
 * at least 1 datapoint.
 *
 * @return [Object]: An array of objects with stops, price, and duration_min,
 * representing each ticket's parameters.
 */
function processResults(data) {
    ticket_options = []

    tickets_results = data.results.products.ticket
    itinerary_results = data.results.itineraries

    for (var itinerary of itinerary_results) {
        segments = itinerary.journeys[0].segments
        stops = segments.length - 1
        departure_time = new Date(segments[0].departure_time)
        arrival_time = new Date(segments[stops].arrival_time)
        duration_min = (arrival_time - departure_time) / (1000 * 60)

        // Iterate through each ticket in the itinerary, and add to the array.
        for (var option of itinerary.product_options[0]) {
            ticket = tickets_results[option.product_id]
            price_info = ticket.price_and_availability.group.price.base
            price = price_info.amount / Math.pow(10, price_info.decimal_places)
            ticket_options.push({
                stops: stops,
                price: price,
                duration_min: duration_min
            })
        }
    }

    return ticket_options
}

// Export functions for testing, but not in the browser.
if (typeof module !== 'undefined') {
    module.exports = {
        processResults: processResults,
        getStats: getStats,
        getTripCreationToken: getTripCreationToken,
        createTrip: createTrip
    }
}
