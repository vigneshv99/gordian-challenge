const search_api = require('../js/search_api.js');
const moment = require('moment')

test('Accurate result processing', () => {
    var testData = {
        results: {
            products: {
                ticket: {
                    "ticket1": {
                        price_and_availability: { group: { price: {
                            base: {
                                amount: 1000,
                                decimal_places: 2
                            }
                        } } }
                    },
                    "ticket2": {
                        price_and_availability: { group: { price: {
                            base: {
                                amount: 2000,
                                decimal_places: 2
                            }
                        } } }
                    }
                }
            },
            itineraries: [
                {
                    journeys: [
                        {
                            segments: [
                                {
                                    departure_time: "2020-10-09T20:50:00Z",
                                    arrival_time: "2020-10-09T21:50:00Z"
                                },
                                {
                                    departure_time: "2020-10-09T22:50:00Z",
                                    arrival_time: "2020-10-09T23:50:00Z"
                                }
                            ]
                        }
                    ],
                    product_options: [
                        [
                            {
                                product_type: "ticket",
                                product_id: "ticket1"
                            }
                        ]
                    ]
                },
                {
                    journeys: [
                        {
                            segments: [
                                {
                                    departure_time: "2020-10-09T22:50:00Z",
                                    arrival_time: "2020-10-09T23:50:00Z"
                                }
                            ]
                        }
                    ],
                    product_options: [
                        [
                            {
                                product_type: "ticket",
                                product_id: "ticket2"
                            }
                        ]
                    ]
                }

            ]
        }
    }

    var groundTruth = [
        {
            price: 10.0,
            stops: 1,
            duration_min: 3 * 60
        },
        {
            price: 20.0,
            stops: 0,
            duration_min: 60
        }
    ]

    expect(search_api.processResults(testData).sort()).toEqual(groundTruth.sort())
});

test('Accurate statistic generation', () => {
    var results = [
        {
            price: 10.0,
            stops: 1,
            duration_min: 3 * 60
        },
        {
            price: 20.0,
            stops: 0,
            duration_min: 60
        },
        {
            price: 120.0,
            stops: 2,
            duration_min: 4 * 60
        }
    ]


    expect(search_api.getStats("price", results)).toEqual({
        min: 10.0,
        max: 120.0,
        average: 50.0,
        dataArr: [10.0, 20.0, 120.0]
    })
    expect(search_api.getStats("stops", results)).toEqual({
        min: 0,
        max: 2,
        average: 1,
        dataArr: [1, 0, 2]
    })

});

test('API endpoints', done => {
    jest.setTimeout(20000);
    search_api.getTripCreationToken(function(success, data) {
        expect(success).toEqual(true)
        expect(typeof(data)).toEqual("string")

        const options = { year: 'numeric', month: '2-digit', day: 'numeric' };

        search_api.createTrip(
            data,
            "SJC",
            "BUR",
            "WN",
            moment().add(10, 'days').format("YYYY-MM-DD"),
            function(success, data) {
                expect(success).toEqual(true)
                expect(data.results).toEqual(expect.anything())
                done()
            }
        )
    });
});
