# Gordian Flight Summaries

## Running the Code
Running the code is simple, just start a simple web server to serve the resources:

`python3 -m http.server 8001`

Then, access `localhost:8001` to see the UI.

## Running the Tests
Run the tests with `npm run tests`, this requires node.js. 
